#SlackAlerts

Send alerts to slack directly from your laravel app

### Installation

TODO: 
- Create a from to send messages to slack


### Usage

Install `composer require shawnsandy/slackalerts`

TODO: 
- Complete usage instructions

### Contributing

Fork it!
Create your feature branch: git checkout -b my-new-feature
Commit your changes: git commit -am 'Add some feature'
Push to the branch: git push origin my-new-feature
Submit a pull request :D
History

### Change Log

TODO: Write history

### The MIT License (MIT)

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
