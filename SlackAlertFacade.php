<?php

namespace Modules\SlackAlerts;
use Illuminate\Support\Facades\Facade;

/**
 * Class SlackAlertFacade
 *
 * @package \Modules\SlackAlerts
 */
class SlackAlertFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'slackalerts';
    }

}