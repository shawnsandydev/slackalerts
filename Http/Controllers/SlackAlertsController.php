<?php

namespace Modules\Slackalerts\Http\Controllers;

use Nwidart\Modules\Routing\Controller;

class SlackAlertsController extends Controller {

	public function index()
	{
		return view('slackalerts::index');
	}

}
