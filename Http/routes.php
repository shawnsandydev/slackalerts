<?php

Route::group(['middleware' => 'web', 'prefix' => 'slackalerts', 'namespace' => 'Modules\SlackAlerts\Http\Controllers'], function()
{
	Route::get('/', 'SlackAlertsController@index');
});