<?php
/**
 * Created by PhpStorm.
 * User: shawnsandy
 * Date: 7/12/16
 * Time: 1:57 PM
 */

namespace Modules\SlackAlerts;

use Maknz\Slack\Client;


class Alerts
{

    protected $client;
    protected $connection;
    protected $hook;

    public function __construct()
    {

    }

    /**
     * @param $hook
     * @param $connection
     * @return $this
     */
    public function connection($hook, $connection) {
        //$connection = new Alerts();
        $this->client = new Client($hook, $connection);
        return $this;
    }
    

    /**
     * @param string $msg
     */
    public function send($msg = 'Slack Alerts: Your Message Should Here')
    {
        $this->client->send($msg);
    }
  

}
